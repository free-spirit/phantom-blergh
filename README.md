# phantom-blergh

A Jekyll theme adaptation of the CC-0 theme and?
[Phantom](https://html5up.net/phantom) by [HTML5Up](https://html5up.net).

## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "phantom-blergh"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: phantom-blergh
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install phantom-blergh

## Usage

Additional (optional) site-wide configuration variables are used and can go in
your `_config.yml`:

```yaml
gitlab_username: my_super_awesome_username

features:
  https_only: true
  email_obfuscation_enabled: false
  readtime_enabled: false

whitelist:
  - jekyll-rot13display
  - jekyll-readtime
```

TODO: Describe available layouts, includes, and/or sass.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/thelonelyghost/phantom-blergh. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, create a new example Jekyll site by running `cd .. && jekyll new pb-example`.

Once the bundle install process is finished, `cd` into the newly created
`pb-example` directory and modify `Gemfile` in the following way:

```diff
  
  # This is the default theme for new Jekyll sites. You may change this to anything you like.
- gem "minima", "~> 2.0"
+ gem "phantom-blergh", path: File.expand_path('../phantom-blergh', File.dirname(__FILE__))
  
  # If you want to use GitHub Pages, remove the "gem "jekyll"" above and
```

Running `bundle exec jekyll serve` from the Jekyll installation will use the
theme just the same as if you installed it via Rubygems. Changes made to the
theme will only be reflected if you restart the Jekyll server, but it gives a
decent way of testing against the default Jekyll installation.

When your theme is released, only the files in `assets`, `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

