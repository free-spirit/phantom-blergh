module Rot13Display
  module Filter
    def rot13(input)
      input.tr('abcdefghijklmnopqrstuvwxyz', 'nopqrstuvwxyzabcdefghijklm')
    end
  end

  class ObfuscatedAnchorTag < Liquid::Tag
    def initialize(tag_name, email, tokens)
      super
      @email = email.to_s
    end

    def render(context)
      placeholder = 'spam@example.com'
      email = ''
      Object.new.tap do |obj|
        obj.include(Filter)
        email = obj.rot13(@email)
      end

      %Q{<a href="mailto:#{placeholder}" data-email="#{email}">#{placeholder}</a>}
    end
  end
end

Liquid::Template.register_filter(Rot13Display::Filter)
Liquid::Template.register_tag('email_tag', Rot13Display::ObfuscatedAnchorTag)
