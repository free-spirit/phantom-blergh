# # jekyll-readtime
# 2015 Ron Scott-Adams, Licensed under MIT:
# https://tldrlegal.com/license/mit-license
# Original work: https://gist.github.com/zachleat/5792681
#
# Outputs the estimated time the average person might take to read the content.
# 200 is a round figure based on estimates gathered from various studies.
# http://www.ncbi.nlm.nih.gov/pubmed/18802819
#
# EXAMPLE:
#
# {% if site.data.layout.readtime_display %}
#   -
#   {% if site.data.layout.readtime_title == 'icon' %}
#     <i class="fa fa-oclock"></i>
#   {% else %}
#     {{ site.data.layout.readtime_title }}
#   {% endif %}
#   {% if page.content | readtime == 0 %}
#     less than a minute
#   {% else %}
#     {{ page.content | readtime | pluralize: 'minute' }}
#   {% endif %}
# {% endif %}
#
module ReadTime
  def readtime(input)
    words_per_minute = 200
    words = input.split.size

    (words / words_per_minute).floor
  end
end

Liquid::Template.register_filter(ReadTime)
