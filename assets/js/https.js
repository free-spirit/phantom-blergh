(function() {
  // Detect dev server environments
  var isDevelopment = window.location.hostname == 'localhost' ||
    (window.location.hostname || '').match(/\.local$/i) ||
    window.location.hostname == '127.0.0.1' ||
    window.location.hostname == '::1';

  // Ensures we're only serving via https, except when localhost development
  if(window.location.protocol != 'https:' && !isDevelopment)
    window.location.protocol = 'https:';
})();
