jQuery(function(){
    jQuery('[data-email]').each(function(){
        var obfuscated_address = jQuery(this).attr('data-email');
        if(typeof obfuscated_address == 'string' && obfuscated_address != '') {
            var clean_email = obfuscated_address.replace(
                /[a-zA-Z]/g,
                function(c){
                    return String.fromCharCode((c<='Z'?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);
                }
            );
            jQuery(this).html(clean_email);
            if(jQuery(this).attr('href') != '') {
                jQuery(this).attr('href', 'mailto:'+clean_email);
            }
            jQuery(this).removeAttr('data-email');
        }
    });
});
