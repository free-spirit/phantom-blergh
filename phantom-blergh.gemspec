# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = 'phantom-blergh'
  spec.version       = '0.2.0'
  spec.authors       = ['David Alexander']
  spec.email         = ['opensource@thelonelyghost.com']

  spec.summary       = 'A Jekyll theme adaptation of https://html5up.net/phantom'
  spec.homepage      = 'https://gitlab.com/thelonelyghost/phantom-blergh'
  spec.license       = 'MIT'

  spec.metadata['plugin_type']        = 'theme'
  spec.metadata['allowed_push_host']  = 'https://rubygems.org'

  spec.files = `git ls-files -z`.split("\x0").select { |f|
    f.match(/^(assets|_layouts|_includes|_sass|LICENSE|README)/i)
  }

  spec.add_runtime_dependency 'jekyll', '~> 3.3'
  # spec.add_runtime_dependency "font-awesome-sass"

  spec.add_development_dependency 'bundler', '~> 1.12'
  spec.add_development_dependency 'rake', '~> 10.0'
end
